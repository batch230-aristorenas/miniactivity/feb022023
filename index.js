console.log("Hello World");


let downPaymentPrice = 2000;

function checkPayment(payment){ // >> A parameter is already provided
    // The value from the argument is passed to the parameter and will be evaluated by the conditonal statement below (if and else. The 'else' block will only run if the condition from the 'if' statement is not satisfied / evaluated false)
    isSuccessful = undefined;
    if(payment < downPaymentPrice){
        isSuccessful = false;
        // >> To make the value of 'isSuccessful' usable outside the function, pass it through a return keyword. 
        // >> Create a return statement here...
            return isSuccessful
        
    }
    else{
        let change = payment - downPaymentPrice;
        console.log("Your change is "+ change);
        isSuccessful = true;
         // >> To make the value of 'isSuccessful' usable outside the function, pass it through a return keyword. 
        // >> Create a return statement here...
        return isSuccessful
    }
}
checkPayment(2500);
console.log(isSuccessful);